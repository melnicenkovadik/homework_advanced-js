for (let i = 1; i <= 9; i++) {
    $.ajax({
        type: "GET",
        url: `https://swapi.co/api/people/?page=${i}`,
        contentType: "json",
        success: function (data) {
            console.log(data.results);
            $.each(data.results, function (index, elem) {
                let content = `<div class="movie-item" id="${elem.name.replace(/ /g, '-')}">
                <p class="item"><strong>Имя:</strong> ${elem.name}</p><p class="item"><strong>Пол:</strong> ${elem.gender}</p>`;
                $.ajax({
                    type: "GET",
                    url: elem.homeworld,
                    contentType: "json",
                    success: function (dataWorld) {
                        content += `<p class="item"><strong>Родной мир:</strong> ${dataWorld.name}</p>`;
                        if (elem.starships.length >= 1) {
                            content += `<button class="button" data-ship="${elem.starships}">Список кораблей
                            </button></div>`;
                        } else {
                            content += `</div>`;
                        }
                        $("#wrapper").append(`${content}`);
                    },
                    error: function (request) {
                        alert(request.status);

                    }
                });
            });
        },
        error: function (request) {
            alert(request.status);
        }
    });
}

$(document).on("click", function (e) {
    let eventTarget;
    let eventId;
    let dataShip;
    if ($(e.target).hasClass("button")) {
        eventTarget = e.target;
        eventId = $(eventTarget).parent().attr('id');
        dataShip = $(eventTarget).attr("data-ship").split(",");
    }
    $.each(dataShip, function (index, elem) {
        $.ajax({
            type: "GET",
            url: elem,
            contentType: "json",
            success: function (dataShip) {
                let hanSolo;
                if (dataShip.name === "Millennium Falcon") {
                    hanSolo = "<h2>Хан Соло стрелял первым!</h2>"
                } else {
                    hanSolo = ""
                }
                $(`#${eventId}`).append(`<a href="#" class="ship-link" data-ship="${dataShip.model}, ${dataShip.starship_class}, 
                    ${dataShip.passengers}, ${dataShip.manufacturer}">${dataShip.name}</a>${hanSolo}`);
            },
            error: function (request) {
                alert(request.status);
            }
        });
    });
    $(eventTarget).replaceWith("<h3>Пилотируемые корабли:</h3>");

    if ($(e.target).hasClass("ship-link")) {
        e.preventDefault();
        eventTarget = e.target;
        dataShip = $(eventTarget).attr("data-ship").split(", ");
        $(eventTarget).append(`<p>Модель: ${dataShip[0]}</p><p>Класс корабля: ${dataShip[1]}</p>
                    <p>Пасажировместимость: ${dataShip[2]}</p><p>Место производства: ${dataShip[3]}</p>`);
        $(eventTarget).css("pointer-events", 'none');
    }
});
