let request = new XMLHttpRequest();
request.open('GET', 'https://swapi.co/api/films/');
request.send();
request.onload = function () {
    if (request.status !== 200) {
        alert(`Ошибка ${request.status}: ${request.statusText}`);
    } else {
        const movieList = JSON.parse(request.response);
        const resultList = movieList.results;
        resultList.forEach(elem => {
            let movieItem = document.createElement("div");
            movieItem.innerHTML =
                `<p class="item"><strong>Movie title:</strong> ${elem.title}</p>
                 <p class="item"><strong>Episode number:</strong> ${elem.episode_id}</p>
                 <p class="item"><strong>Description:</strong> ${elem.opening_crawl}</p>
                 <button class="button">Список персонажей</button>`;
            movieItem.classList.add("movie-item");
            movieItem.id = elem.episode_id;
            document.getElementById("wrapper").appendChild(movieItem);
        });


        const buttons = document.getElementsByClassName("button");
        for(let i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener("click", function(e){
                let eventTarget = e.target;
                const eventMovieId = eventTarget.parentNode.id;

                let characters = document.createElement("p");
                characters.innerHTML = "Список персонажей: ";
                resultList.forEach(elem => {
                    if (elem.episode_id == eventMovieId) {
                        const charactersList = elem.characters;
                        charactersList.forEach(item => {
                            let xhr = new XMLHttpRequest();
                            xhr.open('GET', item);
                            xhr.send();
                            xhr.onload = function () {
                                if (xhr.status !== 200) {
                                    alert(`Ошибка ${xhr.status}: ${xhr.statusText}`);
                                } else {
                                    characters.innerHTML += `${JSON.parse(xhr.response).name}; `;
                                }
                            }
                        });
                    }
                    request.onerror = function () {
                        alert("Произошла ошибка");
                    };
                });
                characters.classList.add("item");
                document.getElementById(eventMovieId).appendChild(characters);
                eventTarget.remove();
            })
        }
    }
};

request.onerror = function () {
    alert("Произошла ошибка");
};
