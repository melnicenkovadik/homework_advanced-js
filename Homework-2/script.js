/*Класс, объекты которого описывают параметры гамбургера.
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании */
class Hamburger {
    constructor(size, stuffing) {
        this._size = size;
        this._stuffing = stuffing;
        this._toppings = [];
    }

    /*Добавить добавку к гамбургеру. Можно добавить несколько добавок, при условии, что они разные.
     * @param topping     Тип добавки
     * @throws {HamburgerException}  При неправильном использовании */

    addTopping(topping) {
        if (!this._toppings.includes(topping)) {
            return this._toppings.push(topping);
        }
    }

    /*Убрать добавку, при условии, что она ранее была добавлена.
  * @param topping   Тип добавки
  * @throws {HamburgerException}  При неправильном использовании */
    removeTopping(topping) {
        return this._toppings = this._toppings.filter(x => x !== topping);
    }

    /*Получить список добавок.
  * @return {Array} Массив добавленных добавок, содержит константы
  *                 Hamburger.TOPPING_*
  */
    get getToppings() {
        return this._toppings;
    }

    /* Узнать размер гамбургера */
    get getSize() {
        return this._size;
    }

    /* Узнать начинку гамбургера */
    get getStuffing() {
        return this._stuffing;
    }

    /*Узнать цену гамбургера
  * @return {Number} Цена в тугриках */
    get calculatePrice() {
        const priceArr = this._toppings.map(x => Hamburger.TOPPINGS[x].price);
        priceArr.push(Hamburger.SIZES[this._size].price, Hamburger.STUFFINGS[this._stuffing].price);
        let price = priceArr.reduce((acc, prices) => acc + prices, 0);
        return price;
    }

    /*Узнать калорийность
 * @return {Number} Калорийность в калориях */
    get calculateCalories() {
        const caloriesArr = this._toppings.map(x => Hamburger.TOPPINGS[x].calories);
        caloriesArr.push(Hamburger.SIZES[this._size].calories, Hamburger.STUFFINGS[this._stuffing].calories);
        let calories = caloriesArr.reduce((acc, itemcalories) => acc + itemcalories, 0);
        return calories;
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = "SIZE_SMALL";
Hamburger.SIZE_LARGE = "SIZE_LARGE";

Hamburger.SIZES = {
    [Hamburger.SIZE_SMALL]: {
        price: 50,
        calories: 20,
    },
    [Hamburger.SIZE_LARGE]: {
        price: 100,
        calories: 40,
    },
};

Hamburger.STUFFING_CHEESE = "STUFFING_CHEESE";
Hamburger.STUFFING_SALAD = "STUFFING_SALAD";
Hamburger.STUFFING_POTATO = "STUFFING_POTATO";

Hamburger.STUFFINGS = {
    [Hamburger.STUFFING_CHEESE]: {
        price: 10,
        calories: 20,
    },
    [Hamburger.STUFFING_SALAD]: {
        price: 20,
        calories: 5,
    },
    [Hamburger.STUFFING_POTATO]: {
        price: 15,
        calories: 10,
    },
};

Hamburger.TOPPING_SPICE = "TOPPING_SPICE";
Hamburger.TOPPING_MAYO = "TOPPING_MAYO";

Hamburger.TOPPINGS = {
    [Hamburger.TOPPING_SPICE]: {
        price: 15,
        calories: 0,
    },
    [Hamburger.TOPPING_MAYO]: {
        price: 20,
        calories: 5,
    },
};


// маленький гамбургер с начинкой из сыра
const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories);
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice);
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice);
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings.length); // 1

